package by.nickzhylach.cettest.koin

import android.content.Context
import by.nickzhylach.cettest.common.isInternetConnected
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.net.ConnectException
import java.util.concurrent.TimeUnit

val appModule = module {
    single { getOkHttpClient(androidApplication()) }
    single { getGson() }
    single { getRetrofit(get(), get()) }
}

fun getClientBuilder(): OkHttpClient.Builder = OkHttpClient.Builder()
    .readTimeout(15, TimeUnit.SECONDS)
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(15, TimeUnit.SECONDS)
    .retryOnConnectionFailure(true)

fun getOkHttpClient(context: Context): OkHttpClient = getClientBuilder()
    .addInterceptor {
        if (context.isInternetConnected()) {
            val response = it.proceed(it.request())
            response
        } else {
            throw ConnectException()
        }
    }.build()

fun getGson(): Gson = GsonBuilder()
    .setLenient()
    .setDateFormat("yyyy-MM-dd HH:mm:ss").create()

fun getRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
    .client(okHttpClient)
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .addConverterFactory(ScalarsConverterFactory.create())
    .addConverterFactory(GsonConverterFactory.create(gson))
    .baseUrl("https://")
    .build()