package by.nickzhylach.cettest.common

import android.net.Uri
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import by.nickzhylach.cettest.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("circleImageRes")
fun setCircleImageRes(imageView: ImageView, imageResId: Int) {
    var requestOptions = RequestOptions()
    requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(imageView.rootView.width / 2))
    Glide.with(imageView.context)
        .load(imageResId)
        .apply(requestOptions)
        .into(imageView)
}

@BindingAdapter("circleAvatarUrl")
fun setCircleImageUrl(imageView: ImageView, imageUrl: String?) {
    var requestOptions = RequestOptions()
    val radius = (if (imageView.rootView.width > imageView.rootView.height) imageView.rootView.height else imageView.rootView.width) / 2
    requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(radius))
    if (imageUrl != null && imageUrl.isNotEmpty()) Glide.with(imageView.context)
        .load(imageUrl)
        .apply(requestOptions)
        .into(imageView)
    else Glide.with(imageView.context)
        .load(R.drawable.ic_android)
        .apply(requestOptions)
        .into(imageView)
}

@BindingAdapter("textByString")
fun setTextByString(textView: TextView, text: String?) {
    text?.let {
        if (textView.text.toString() != text)
        textView.text = it
    }?: run {
        textView.text = ""
    }
}

@BindingAdapter("imageUri")
fun loadImageByUri(iv: ImageView, uri: Uri?) {
    if (uri != null) {
        Glide.with(iv.context)
                .load(uri)
                .into(iv)
    } else {
        iv.setImageDrawable(null)
    }
}