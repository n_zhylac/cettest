package by.nickzhylach.cettest.api

import retrofit2.Response
import retrofit2.http.GET

interface ImagesAPI {
    @GET("picsum.photos/1920/1080")
    suspend fun get1920x1080(): Response<String>

    @GET("picsum.photos/640/360")
    suspend fun get640x360(): Response<String>
}