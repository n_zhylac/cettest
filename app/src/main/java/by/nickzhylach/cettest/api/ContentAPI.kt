package by.nickzhylach.cettest.api

import androidx.annotation.IntRange
import by.nickzhylach.cettest.model.Content
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ContentAPI {
    @GET("jsonplaceholder.typicode.com/posts")
    suspend fun getPage(
        @Query("page") @IntRange(from = 1) page: Int = 1
    ): Response<List<Content>>
}