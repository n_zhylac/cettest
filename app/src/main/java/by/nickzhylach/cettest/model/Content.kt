package by.nickzhylach.cettest.model

data class Content(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String,
    var image: String? = null
) {
    val stringId = "ID: $id"
    val stringUserId = "User ID: $userId"
}