package by.nickzhylach.cettest.services

import androidx.paging.PagingSource
import androidx.paging.PagingState
import by.nickzhylach.cettest.model.Content
import retrofit2.HttpException

class ContentPageSource(
    private val contentService: IContentService
): PagingSource<Int, Content>() {
    override fun getRefreshKey(state: PagingState<Int, Content>): Int? {
        val anchorPosition = state.anchorPosition ?: return null
        val anchorPage = state.closestPageToPosition(anchorPosition) ?: return null
        return anchorPage.prevKey?.plus(1) ?: anchorPage.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Content> {
        try {
            val pageNumber = params.key ?: INITIAL_PAGE_NUMBER
            val pageContent = contentService.page(pageNumber)

            return if (pageContent != null) {
                val nextPageNumber = if (pageContent.isEmpty()) null else pageNumber + 1
                val prevPageNumber = if (pageNumber > 1) pageNumber - 1 else null
                LoadResult.Page(pageContent, prevPageNumber, nextPageNumber)
            } else {
                LoadResult.Error(Exception("Data Error!"))
            }
        } catch (e: HttpException) {
            return LoadResult.Error(e)
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

    interface Factory {

        fun create(): ContentPageSource
    }

    companion object {
        const val INITIAL_PAGE_NUMBER = 1
    }
}