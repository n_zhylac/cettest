package by.nickzhylach.cettest.services

import by.nickzhylach.cettest.model.Content

interface IContentService {
    suspend fun page(page: Int = 1): List<Content>?
}