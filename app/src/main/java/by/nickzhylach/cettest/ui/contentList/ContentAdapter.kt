package by.nickzhylach.cettest.ui.contentList

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import by.nickzhylach.cettest.R
import by.nickzhylach.cettest.databinding.IContentBinding
import by.nickzhylach.cettest.model.Content

class ContentAdapter(
    context: Context,
    private val items: ArrayList<Content>
): PagingDataAdapter<Content, ContentAdapter.ViewHolder>(ContentDiffItemCallback) {
    private val layoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(layoutInflater.inflate(R.layout.i_content, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val viewBinding by viewBinding(IContentBinding::bind)
        fun bind(content: Content) {
            with(viewBinding) {
                
            }
        }
    }
}

private object ContentDiffItemCallback : DiffUtil.ItemCallback<Content>() {

    override fun areItemsTheSame(oldItem: Content, newItem: Content): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Content, newItem: Content): Boolean {
        return oldItem.userId == newItem.userId && oldItem.id == newItem.id
    }
}