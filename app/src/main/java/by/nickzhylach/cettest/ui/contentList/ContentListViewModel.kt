package by.nickzhylach.cettest.ui.contentList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import by.nickzhylach.cettest.model.Content
import by.nickzhylach.cettest.repository.IContentRepository
import by.nickzhylach.cettest.repository.IImageRepository
import by.nickzhylach.cettest.services.ContentPageSource
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn

class ContentListViewModel(
    private val contentRepository: IContentRepository,
    private val imageRepository: IImageRepository,
    private val contentPageSource: ContentPageSource.Factory
): ViewModel() {
    val contentFlow: StateFlow<PagingData<Content>> = Pager<Int, Content>(
        PagingConfig(20),
    ) {
        contentPageSource.create()
    }.flow
        .stateIn(viewModelScope, SharingStarted.Lazily, PagingData.empty())
}