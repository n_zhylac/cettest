package by.nickzhylach.cettest.ui.contentList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import by.nickzhylach.cettest.R
import by.nickzhylach.cettest.databinding.FContentListBinding
import by.nickzhylach.cettest.model.Content

class ContentListFragment: Fragment() {
    private lateinit var binding: FContentListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FContentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.adapter = ContentAdapter(
            arrayListOf(
                Content(1, 1, "sfgv", "kahgf", "https://picsum.photos/600/300"),
                Content(2, 1, "sfj", "kcvbdghnahgf", "https://picsum.photos/600/300"),
                Content(3, 1, "sxcfsadfgv", "kaxcfbshgf", "https://picsum.photos/600/300"),
                Content(4, 1, "sjzffgv", "kahbzxcvbgf", "https://picsum.photos/600/300"),
                Content(5, 1, "asgsfgv", "kazdsfgsdhjhgf")
            )
        )
    }
}